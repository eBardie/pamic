[[_TOC_]]

## Overview

**pamic** simplifies configuring Pulseaudio in one of two modes:

- Echo cancellation (combined with some level of noise suppression)

  Echo cancellation mode is for when you need to use a microphone with ordinary
  speakers. For example, your system has speakers attached, you're not using
  headphones, your microphone is picking up the sound coming from the speakers
  and echoing it back out to everyone else on the conference call! 

  The Pulseaudio echo cancellation module can also perform noise suppression,
  and **pamic** makes use of that when in echo cancellation mode. It's not
  quite as capable as the freestanding noise suppression, but using it saves
  the overhead of using two separate modules.

- Standalone noise suppression

  Standalone noise suppression mode is for when you are using headphones, but
  your microphone is picking up unwanted background noise. In my case this is
  my laptop's fan, but it's also good for office hubbub, crowds, construction,
  planes, trains, automobiles...

**pamic** affects the live configuration, it does not change any stored config
files, i.e. its effects are not persistent. This means you'll need to run
**pamic** each time you log in.


## Installation 

Download [pamic](https://gitlab.com/eBardie/pamic/-/raw/master/pamicg/pamic) to somewhere in your system's PATH. `~/.local/bin` should be good. Make it executable:

	mv ~/Download/pamic ~/.local/bin
	chmod 755 ~/.local/bin/pamic

Echo cancellation is performed by a standard Pulse audio module, so if you're running the latest version of Pulseaudio that should be covered.

Note: I've only tested **pamic** on Pulseaudio version 13.0. If you're using a different version, please let me know how it goes.

For standalone noise suppression **pamic** makes use of the marvellous https://github.com/werman/noise-suppression-for-voice

Download the latest `linux_rnnoise_bin_x64.tar.gz` tarball from https://github.com/werman/noise-suppression-for-voice/releases/

Extract `librnnoise_ladspa.so` and place it in `/usr/local/lib/`:

	tar xf ~/Downloads/linux_rnnoise_bin_x64.tar.gz bin/ladspa/librnnoise_ladspa.so
	sudo mv librnnoise_ladspa.so /usr/local/lib/


## Using pamic

### List available input and output devices

To discover what sources and sinks are available, use `pamic --list` (or `pamic -L`):

	$ pamic --list
	sources:
	  alsa_input.pci-0000_00_1f.3.analog-stereo                                module-alsa-card.c  s16le  2ch  44100Hz  RUNNING
	  alsa_output.usb-M-Audio_FastTrack_Pro-00.analog-stereo-a-output.monitor  module-alsa-card.c  s24be  2ch  48000Hz  RUNNING
	  alsa_input.usb-M-Audio_FastTrack_Pro-00.analog-stereo-a-input            module-alsa-card.c  s24be  2ch  48000Hz  SUSPENDED
	  alsa_input.pci-00.3.analog-stereo                                        module-alsa-card.c  s16le  2ch  44100Hz  RUNNING
	  alsa_output.pci-00.1.hdmi-stereo.monitor                                 module-alsa-card.c  s16le  2ch  44100Hz  RUNNING

	sinks:
	  alsa_output.usb-M-Audio_FastTrack_Pro-00.analog-stereo-a-output  module-alsa-card.c  s24be  2ch  48000Hz  IDLE
	  alsa_output.pci-00.1.hdmi-stereo                                 module-alsa-card.c  s16le  2ch  44100Hz  IDLE

	modules loaded by pamic:

### Echo cancellation (with some noise suppression)

To remove the sound coming out of the speakers attached to *alsa_output.usb-M-Audio_FastTrack_Pro-00.analog-stereo-a-output* from 
the microphone signal coming in on *alsa_input.pci-0000_00_1f.3.analog-stereo*:

	pamic -s alsa_input.pci-0000_00_1f.3.analog-stereo -S alsa_output.usb-M-Audio_FastTrack_Pro-00.analog-stereo-a-output -l

Applications needing their sound subtracting from the microphone signal should now use the **pamic virtual speaker** sink for their output. See [Configuring Individual Applications](#configuring-individual-applications) below.

Applications needing the microphone should use the **pamic virtual mic** source for their input. This should appear in the list of available microphones in most relevant apps. 

### Noise suppression only

To suppress noise on the microphone signal coming in on *alsa_input.pci-0000_00_1f.3.analog-stereo*, setting the incoming recording level to 30 percent:

	pamic -s alsa_input.pci-0000_00_1f.3.analog-stereo --vin 30 -l

Applications needing the microphone should use the **pamic virtual mic** source for their input. This should appear in the list of available microphones in most relevant apps. 

### Unload pamic

To stop **pamic**:

	pamic -u

### Configuring individual applications

Once **pamic** has configured its new virtual devices, any freshly started applications should use **pamic virtual speaker** as their sound output device. 

You may still need to change the routing for applications which are already connected to a different output device, or for applications which have been configured to use a specific output.

I usually use [pavucontrol](https://freedesktop.org/software/pulseaudio/pavucontrol/) to control which app uses what output. If you're using Pulseaudio, **pavucontrol** (or something similar) is probably already installed. 

![Change output (sink) in pavucontrol](pamicg/img/pavucontrol_select_sink.gif)

### Using pamic *and* PulseEffects

[PulseEffects](https://github.com/wwmm/pulseeffects) makes it a simple matter to extensively tweak the sound coming through PulseAudio.

I use it to boost bass, add some [audio excitation](https://en.wikipedia.org/wiki/Exciter_(effect)), and [fettle](https://en.wiktionary.org/wiki/fettle#Verb) the stereo image.

The gotcha here is that **pamic**'s **Echo-Cancel Sink Stream** *must* be blacklisted, otherwise Pulseaudio will detect a loop and there'll be only silence.

1. In **PulseEffects**'s main tab...
2. select **Applications**...
3. find the **Echo-Cancel Sink Stream** item and...
4. click its **Blacklist** button.
5. Restart **PulseEffects**

(**pamic** and **PulseEffects** must both be running of course.)

![Blacklist 'Echo-Cancel Sink Stream'](pamicg/img/pulseeffects_blacklist_echo_cancel_sink_stream.jpg)

*[This image is of PulseEffects 4.8.0, using some GTK theme or other, and displayed in AwesomeWM. YMWV😄]*


## More information

**pamic** has a lot of help info built in, and rather than duplicate that here I suggest reading it there: `pamic --help`. Alternatively scroll down to around [line 300](https://gitlab.com/eBardie/pamic/-/blob/master/pamic#L300-484) in the source.


## Suggestions/problems?

Please open an [issue](https://gitlab.com/eBardie/pamic/-/issues).


## Ingredients sources

Microphone icon from [free-icon-download.com](http://free-icon-download.com/modules/PDdownloads/singlefile.php?cid=17&lid=67).
