# *_DIR variables refer to single level subdirectories of the top level topplot
# source directory. This simplifies the sed usage throughout this file.
BUILD_DIR:=build
DIST_DIR:=dist
ENV_DIR:=envdir
TEST_EXE:=$(ENV_DIR)/bin/pamicg
PAMIC_VERSION:=$(shell sed -n 's/^\s*version = \(.*\)$$/\1/p' setup.cfg)
PAMIC_WHEEL:=$(DIST_DIR)/pamic-$(PAMIC_VERSION)-py3-none-any.whl

#-------------------------------------------------------------------------------

.PHONY: build clean test test-upload upload

.ONESHELL: # Allow multiline recipes

#-------------------------------------------------------------------------------

clean:
	rm -rf $(BUILD_DIR) $(DIST_DIR) $(ENV_DIR) $(TEST_DIR) *.egg-info

#-------------------------------------------------------------------------------

$(PAMIC_WHEEL): pamicg/*.py pamicg/pamicg.glade setup.cfg pyproject.toml
	python3 -V
	#python3 setup.py sdist bdist_wheel
	python3 -m build -s -w

build: $(PAMIC_WHEEL)

#-------------------------------------------------------------------------------

$(TEST_EXE): $(PAMIC_WHEEL)
	[ -d $(ENV_DIR) ] && rm -rf $(ENV_DIR)
	python3 -m venv $(ENV_DIR)
	. $(ENV_DIR)/bin/activate
	pip3 install pygobject
	pip3 install $(PAMIC_WHEEL)

install: $(TEST_EXE)

#-------------------------------------------------------------------------------

test: $(TEST_EXE)

#-------------------------------------------------------------------------------

test-upload: $(PAMIC_WHEEL)
	echo ebardie | twine upload --verbose --repository-url https://test.pypi.org/legacy/ $(DIST_DIR)/*
	sleep 3

upload: clean $(PAMIC_WHEEL) $(PNG_FILES)
	echo ebardie | twine upload --verbose  $(DIST_DIR)/*

force-upload: $(PAMIC_WHEEL)
	echo ebardie | twine upload --verbose  $(DIST_DIR)/*

#-------------------------------------------------------------------------------
